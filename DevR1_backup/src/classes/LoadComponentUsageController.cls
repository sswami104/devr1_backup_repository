/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  31/07/2014
 * Description: This Class does below processing
 *              1. Displays which users are using the selected component.
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * Raymundo Rodriguez        31/07/2014              Initial version.
 **************************************************************************************/
global with sharing class LoadComponentUsageController {
    
    //Global variables
    public Development_Change__c currentChangeRecord {get; set;}
    public List<Development_Change__c> listOfComponents {get; set;}
    public Boolean foundResults {get; set;}
    public Boolean isOpen {get; set;}
    private Set<String> listOfStatus = new Set<String>();
    
    
    /*
    * Method name  :   LoadComponentUsageController
    * Description  :   Constructor of the class.
    * Return Type  :   -
    * Parameter    :   Controller.
    */
    public LoadComponentUsageController(ApexPages.StandardController controller) {
        currentChangeRecord = (Development_Change__c) controller.getRecord();
        for(Development_Change__c loadedChange : [SELECT Id, User_Story__c, Component__r.Name, Status__c FROM Development_Change__c WHERE Id = :currentChangeRecord.Id]) {
            currentChangeRecord = loadedChange;
        }
        listOfComponents = new List<Development_Change__c>();
        listOfStatus.add('Not Started');
        listOfStatus.add('In Progress');
        listOfStatus.add('On Hold');
        isOpen = listOfStatus.contains(currentChangeRecord.Status__c);
        loadUsage();
    }
    
    /*
    * Method name  :   loadUsage
    * Description  :   Loads the current usage of the component.
    * Return Type  :   void
    * Parameter    :   -
    */
    public void loadUsage() {
        listOfComponents = [SELECT Id, Name, CreatedBy.Name, Status__c, User_Story__c, User_Story__r.Name FROM Development_Change__c 
                WHERE User_Story__c = :currentChangeRecord.User_Story__c AND Status__c IN :listOfStatus AND Id <> :currentChangeRecord.Id];
        if(listOfComponents.size() > 0) {
            foundResults = true;
        } else {
            foundResults = false;
        }
    }
}