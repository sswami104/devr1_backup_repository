/************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  15/04/2013
 * Description: This Trigger does below processing
 *              1. Creates Tasks upon Change record creation.
 *
 *  Modification Log:
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                Description
 *  * ---------------------------------------------------------------------------
 *  * Raymundo Rodriguez        15/04/2014          Initial version.
 ************************************************************************************/
trigger ChangeTrigger on Development_Change__c(after insert) {
    
    //After Insert
    if(Trigger.isAfter && Trigger.isInsert) {
        List<Task> newTasks = new List<Task>();
        for(Development_Change__c change : Trigger.new){
            
            //Creating Task.
            Task newTask = new Task();
            newTask.WhatId = change.Id;
            newTask.OwnerId = change.CreatedById;
            newTask.Subject = 'Attach backup and result';
            newTask.Description = 'The backup of the code changed or components changed must be attached to the Change record, as well as the code finished or components finished.';
            newTasks.add(newTask);
        }
        
        //Inserting new records.
        insert newTasks;
    }
}